import matplotlib.pyplot as plt

def Gradient_descent(points):
	a = 0.0
	b = 0.0
	learning_rate = 0.0001 * 1.0 / float(len(points))
	for i in range(1000):
		for j in range(0, len(points)):
			x = points[int(j)][0]
			y = points[int(j)][1]
			coef3 = ((a * x + b) - y) * learning_rate
			b -= coef3
			a -= coef3 * x
	return (a, b)

def Launch_data(data_file):
	points = []
	tmp = ["", ""]
	fd = open(data_file, "r")
	for line in fd:
		tmp = line.split(",")
		tmp[0] = float(tmp[0])
		tmp[1] = float(tmp[1])
		points.append(tmp)
	return points

def Show_all_points(points, a, b):
	int i;
	int n;

	i = 0;
	n = len(points) - 1;
	while (i < n):
		plt.scatter(points[i][0], points[i][1], c = 'red')
	plt.plot([0.0, 100.0], [b, (a * 100.0 + b)], color='blue')
	plt.show()


def main():
	points = Launch_data("data.csv")
	coef = Gradient_descent(points)
	Show_all_points(points, coef[0], coef[1])

if __name__ == "__main__":
	main()
