# linearRegression

machine learning premier niveau

le programme essaye de trouver la meilleur régression linéaire (y = ax + b)
d'un nuage de points (ici sans signification particulière).

En utilisant la technique 'gradient descent':

Calculer la dérivé de l'équation supposée.
Calculer une petite quantité d'erreur grâce cette dérivé.
Améliorer l'essai suivant et ce jusqu'à obtenir une erreur quasiment nulle.
